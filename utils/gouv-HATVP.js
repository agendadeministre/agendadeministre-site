var fetch = require('../lib/fetch-n-save');
var fs = require('fs').promises;
var path = require('path');
const parse = require('csv-parse')

/*[
  'civilite',
  'prenom',
  'nom',
  'classement',
  'type_mandat',
  'qualite',
  'type_document',
  'departement',
  'date_publication',
  'nom_fichier',
  'url_dossier',
  'open_data',
  'date_depot',
  'id_origine',
  'url_photo'
]*/


async function scrapHATVP() {
	try {
        const gouvBuffer = await fs.readFile(path.join(__dirname, '../data/gouv.json'));
        const gouvJson = gouvBuffer.toString();
        const gouv = JSON.parse(gouvJson);
        var db = {};
        for(g in gouv){
          console.log(gouv[g]['prenom'] + " " + gouv[g]['nom'] + " already in database.");
          db[gouv[g]['prenom'] + " " + gouv[g]['nom']] = gouv[g];
        }
      console.log(db);
        
		const res = await fetch.simple('https://www.hatvp.fr/livraison/opendata/liste.csv');

		if (res.status >= 400) {
			throw new Error("Bad response from server");
		}

		var data = await res;
		parse(data, {
			delimiter: ";",
			comment: '#',
			columns: true
		}, function (err, output) {
			for (i in output) {
				if (output[i]['type_mandat'] == "gouvernement") {
					if (!db[output[i]['prenom'] + " " + output[i]['nom']]) {
						var id = output[i]['nom'].toLowerCase().replace(/ /g, "-").replace(/é/g,"e").replace(/è/g,"e")+"-"+output[i]['prenom'].toLowerCase().replace(/ /g, "-").replace(/é/g,"e").replace(/è/g,"e");
						db[output[i]['prenom'] + " " + output[i]['nom']] = {
							id: id,
							civilite: output[i]['civilite'],
							prenom: output[i]['prenom'],
							nom: output[i]['nom'],
							qualite: output[i]['qualite'],
							photo: "https://www.hatvp.fr/livraison/photos_gouvernement/"+id+".jpg",
                            scrapped: 0
						};
                        console.log(output[i]['prenom'] + " " + output[i]['nom'] + " added to the database.");
					}
				}
			}
			fs.writeFile(path.join(process.cwd(), 'data', 'gouv-raw.json'), JSON.stringify(Object.values(db), null, 2), function (err) {
			  if (err){
				  return console.log(err);
			  }else{
				  console.log("Saved !");
			  }

			});
		})

	} catch (err) {
		console.error(err);
	}
}


scrapHATVP();
