const console = require('../lib/log-n-save');
const fs = require('fs');
const path = require('path');
const ical = require('ical-generator');
const express = require('express');
const serveIndex = require('serve-index');
const asyncHandler = require('express-async-handler');
const ejs = require('ejs');
const app = express();
const moment = require('moment');
const cron = require('./cron.js');

global.port = process.env.PORT;
if (!process.env.PORT) {
	port = 3030;
}
global.env = process.env.ENV;
if (!process.env.ENV) {
	env = "prod";
}
global.cronTime = process.env.CRON;
if (!process.env.CRON) {
	cronTime = 60 * 60 * 1000;
}

global.logPath = "log-" + moment().format('X') + ".log";

fs.writeFile('logs/' + global.logPath, moment().format() +" : INIT logs\n",
	function (err) {
		if (err) {
			return console.log(err);
		}

	});

console.log("Lancement du serveur...");
console.log("- environnement : " + env);
console.log("- port          : " + port);
console.log("- cron          : " + cronTime);
console.log("- logs          : " + global.logPath);
console.log("-----------------------");


app.use('/tui-code-snippet', express.static('node_modules/tui-code-snippet/dist'))
app.use('/tui-calendar', express.static('node_modules/tui-calendar/dist'))
app.use('/tui-date-picker', express.static('node_modules/tui-date-picker/dist'))
app.use('/tui-time-picker', express.static('node_modules/tui-time-picker/dist'))

app.use('/', express.static('front'))
app.use('/data', express.static('data'), serveIndex('data', {
	'icons': true
}))
app.use('/archives', express.static('archives'), serveIndex('archives', {
	'icons': true
}))
app.use('/logs', express.static('logs'), serveIndex('logs', {
	'icons': true
}))


app.set('view engine', 'ejs');

var gouv = [];
var gouvSearch = {};
var filePath = path.join(process.cwd(), 'data/gouv.json');
fs.readFile(filePath, {
	encoding: 'utf-8'
}, function (err, data) {
	if (err) {
		console.log(err);
	} else {
		gouv = JSON.parse(data);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}
	}
});

app.get('/', (req, res) => {
	res.render('index', {
		gouv: gouv
	});
});


app.get('/gouv.ics', asyncHandler(async (req, res, next) => {
	var calendarData = [];
	for (g in gouv) {
		if (gouv[g].dispo == 1) {
			try {
				const data = await fs.promises.readFile('data/' + gouv[g].id + '.json');
				calendarData = calendarData.concat(JSON.parse(data));
			} catch (err){
				console.log(err);
			}

		}
	}

	var calendar = ical({
		domain: 'agendadeministre.fr',
		name: 'Agenda du gouvernement'
	});

	for (c in calendarData) {
		if (calendarData[c]) {
			var attendees = []
			for (a in calendarData[c].attendees) {
				attendees.push({
					email: 'noreply@agendadeministre.fr',
					name: calendarData[c].attendees[a]
				});
			}
			try {
				calendar.createEvent({
					start: calendarData[c].start,
					end: calendarData[c].end,
					summary: calendarData[c].title,
					location: calendarData[c].location,
					attendees: attendees
				});
			} catch (err) {
				console.log(err);
				console.log({
					start: calendarData[c].start,
					end: calendarData[c].end,
					summary: calendarData[c].title,
					location: calendarData[c].location,
					attendees: attendees
				});
			}
		}
	}

	calendar.serve(res)
}));

app.get('/gouv', (req, res) => {
	var files = [];
	for (g in gouv) {
		if (gouv[g].dispo == 1) {
			files.push('\"/data/' + gouv[g].id + '.json\"');
		}
	}
	res.render('calendar', {
		files: files,
		title: "Agenda du gouvernement",
		id: "gouv"
	});
});

app.get('/:id.ics', (req, res) => {
	if (gouvSearch[req.params.id]) {
		var filePath = path.join(process.cwd(), 'data/' + req.params.id + '.json');
		fs.readFile(filePath, {
			encoding: 'utf-8'
		}, function (err, data) {
			if (err) {
				console.log(err);
			} else {
				calendarData = JSON.parse(data);

				var calendar = ical({
					domain: 'agendadeministre.fr',
					name: 'Agenda de ' + gouvSearch[req.params.id].civilite + ' ' + gouvSearch[req.params.id].prenom + ' ' + gouvSearch[req.params.id].nom
				});

				for (c in calendarData) {
					if (calendarData[c]) {
						var attendees = []
						for (a in calendarData[c].attendees) {
							attendees.push({
								email: 'noreply@agendadeministre.fr',
								name: calendarData[c].attendees[a]
							});
						}
						try {
							calendar.createEvent({
								start: calendarData[c].start,
								end: calendarData[c].end,
								summary: calendarData[c].title,
								location: calendarData[c].location,
								attendees: attendees
							});
						} catch (err) {
							console.log(err);
							console.log({
								start: calendarData[c].start,
								end: calendarData[c].end,
								summary: calendarData[c].title,
								location: calendarData[c].location,
								attendees: attendees
							});
						}
					}
				}

				calendar.serve(res)
			}
		});
	} else {
		res.send("404");
	}
});

app.get('/:id', (req, res) => {
	if (gouvSearch[req.params.id]) {
		res.render('calendar', {
			files: ['\"/data/' + req.params.id + '.json\"'],
			title: "Agenda de " + gouvSearch[req.params.id].civilite + " " + gouvSearch[req.params.id].prenom + " " + gouvSearch[req.params.id].nom,
			id: req.params.id
		});
	} else {
		res.send("404");
	}
});


app.listen(port, () => console.log(`Serveur disponible sur http://localhost:${port}`))

setInterval(function () {
	cron.execute();
}, cronTime);
