#!/bin/env node

/* eslint-disable no-continue, no-restricted-syntax */
const fs = require('fs-extra');
const path = require('path');
const mkdirp = require('mkdirp');

(async () => {
  const gouvBuffer = await fs.readFile(path.join(__dirname, '../data/gouv.json'));
  const gouvJson = gouvBuffer.toString();
  const gouv = JSON.parse(gouvJson);

  await mkdirp(path.join(process.cwd(), 'data', 'agendas'));
  await mkdirp(path.join(process.cwd(), 'logs'));
  await mkdirp(path.join(process.cwd(), 'archives'));

  for await (const {
    id
  } of gouv) {
    const scraperPath = `../scrapers/${id}.js`;
    let scraper;

    let exists = false;
    try {
      await fs.stat(path.join(__dirname, scraperPath));
    } catch (e) {
      console.error(`No scraper for ${id}`);
      continue;
    }

    try {
      scraper = require(scraperPath);
    } catch (e) {
      console.error(`Error requiring scrapper for ${id}`);
      console.error(e);
      continue;
    }

    try {
      console.log("[" + id + "] : Starting scrapping…");
      var calendar = await scraper.scrap();
      console.log("[" + id + "] : " + (calendar ? calendar.length : 0) + " events scrapped !");
    } catch (e) {
      console.log(`Error scraping data for ${id}`);
      console.error(e);
      continue;
    }

    if (calendar) {
      try {
        console.log("[" + id + "] : Saving data…");
        var filePath = path.join(process.cwd(), 'data', 'agendas', id + '.json');
        fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
          if (err) {
            return console.log(id + " " + err);
          } else {
            console.log("[" + id + "] : " + calendar.length + " events saved !");
          }

        });
      } catch (e) {
        console.log(`Error saving data for ${id}`);
        console.error(e);
        continue;
      }
    }
  }
})();