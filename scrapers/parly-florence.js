var minDefenseScrapper = require('./common/min-defense');

var calendarId = 'parly-florence';
var rssLink = 'https://www.defense.gouv.fr/dynrss/feed/517137';

var scrap = function () {
	return minDefenseScrapper.scrap(calendarId, rssLink);
}

exports.scrap = scrap;
