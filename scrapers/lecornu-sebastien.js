var minDefenseScrapper = require('./common/min-defense');

var calendarId = 'lecornu-sebastien';
var rssLink = 'https://www.defense.gouv.fr/dynrss/feed/517137'; //transformation de HTML en PDF: https://www.defense.gouv.fr/salle-presse/20072022-agenda-sebastien-lecornu-ministre-armees-semaine-30

var scrap = function () {
	return minDefenseScrapper.scrap(calendarId, rssLink);
}

exports.scrap = scrap;
