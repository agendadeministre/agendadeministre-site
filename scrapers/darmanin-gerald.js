var minInterieurScrapper = require('./common/min-interieur');

var calendarId = 'darmanin-gerald';
var baseLink = 'https://www.interieur.gouv.fr/fr/Le-ministre/Agenda-du-ministre';

var scrap = function () {
	return minInterieurScrapper.scrap(calendarId, baseLink);
}

exports.scrap = scrap;
