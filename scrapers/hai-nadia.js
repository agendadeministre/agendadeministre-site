var minCohesionTerritoiresScrapper = require('./common/min-cohesion-territoires');

var calendarId = 'hai-nadia';
var minId = 'nadia-hai';

var scrap = function () {
	return minCohesionTerritoiresScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
