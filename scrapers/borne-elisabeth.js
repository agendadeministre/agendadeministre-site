var fetch = require('../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
const parse = require('csv-parse')
var moment = require('moment');

/*
date,heure,titre,lieu
*/

var scrap = async function () {
  var calendar = [];
  var calendarId = 'borne-elisabeth';
  try {
    var gouv = [];
    var gouvSearch = {};
    var filePath = path.join(process.cwd(), 'data', 'gouv.json');
    const gouvData = await fs.promises.readFile(filePath);
    gouv = JSON.parse(gouvData);
    for (g in gouv) {
      gouvSearch[gouv[g].id] = gouv[g];
    }

    var now = moment();
    if (now.day() == 0) {
      var premierJourDeLaSemaine = now.subtract(1, 'weeks').day(0);
    } else {
      var premierJourDeLaSemaine = now.day(0);
    }
    const res = await fetch.nSave('https://www.gouvernement.fr/agenda/csv');

    var data = await res;

    if (data) {
      parse(data, {
        delimiter: ",",
        comment: '#',
        columns: true
      }, function (err, output) {
        for (i in output) {
          switch (output[i]['heure']) {
            case "Matin":
              var startDate = moment(output[i]['date'] + " 9:00", "DD/MM/YYYY HH:mm");
              var endDate = moment(output[i]['date'] + " 12:00", "DD/MM/YYYY HH:mm").add(1, 'h');
              break;
            case "Après-midi":
              var startDate = moment(output[i]['date'] + " 14:00", "DD/MM/YYYY HH:mm");
              var endDate = moment(output[i]['date'] + " 17:00", "DD/MM/YYYY HH:mm").add(1, 'h');
              break;
            default:
              var startDate = moment(output[i]['date'] + " " + output[i]['heure'], "DD/MM/YYYY HH:mm");
              var endDate = moment(output[i]['date'] + " " + output[i]['heure'], "DD/MM/YYYY HH:mm").add(1, 'h');
              break;
          }

          calendar.push({
            id: calendarId + "-" + i,
            calendarId: calendarId,
            title: output[i]['titre'],
            category: 'time',
            start: startDate.format(),
            end: endDate.format(),
            location: output[i]['lieu'],
            attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
          });
        }
        //console.log(calendar);
        return calendar;
      })
    }


  } catch (err) {
    console.error(err);
  }
}

exports.scrap = scrap;