var minInterieurScrapper = require('./common/min-interieur');

var calendarId = 'nunez-laurent';
var baseLink = 'https://www.interieur.gouv.fr/fr/Le-secretaire-d-Etat/Agenda-du-secretaire-d-Etat';

var scrap = function () {
	return minInterieurScrapper.scrap(calendarId, baseLink);
}

exports.scrap = scrap;
