var minEcoloSolidaireScrapper = require('./common/min-ecologique-solidaire');

var calendarId = 'pannier-runacher-agnes';
var minId = 'dagnes-pannier-runacher';

var scrap = function () {
	return minEcoloSolidaireScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
