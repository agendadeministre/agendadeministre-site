var fetch = require('../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
const parse = require('csv-parse')
var moment = require('moment');

/*
date,heure,titre,lieu
*/

var scrap = async function () {
	var calendar = [];
	var calendarId = 'castex-jean';
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		var now = moment();
		if (now.day() == 1) {
			var premierJourDeLaSemaine = now.subtract(1, 'weeks').day(1);
		} else {
			var premierJourDeLaSemaine = now.day(1);
		}
		var csvKey = premierJourDeLaSemaine.format("YYYY-MM-DD");
		const res = await fetch.nSave('https://www.transformation.gouv.fr/la-ministre/agenda-de-la-ministre/export/csv?date=' + csvKey);

		var data = await res;

		if (data) {
			parse(data, {
				delimiter: ",",
				comment: '#',
				columns: true
			}, function (err, output) {
				for (i in output) {
					var startDate = moment(output[i]['field_date_et_heure'], "DD/MM/YYYY HH\hmm"); //this scrapper does not work right now because the CSV is incomplete
					var endDate = moment(output[i]['field_date_et_heure'], "DD/MM/YYYY HH\hmm").add(1, 'h');
					calendar.push({
						id: calendarId + "-" + i,
						calendarId: calendarId,
						title: output[i]['title'],
						category: 'time',
						start: startDate.format(),
						end: endDate.format(),
						location: output[i]['field_lieu'],
						attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
					});
				}
				//console.log(calendar);
                return calendar;
			})
		}


	} catch (err) {
		console.error(err);
	}
}

exports.scrap = scrap;
