var fetch = require('../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
const jsdom = require("jsdom");
const {
	JSDOM
} = jsdom;
var jQuery = require('jquery');
var moment = require('moment');


var scrap = async function () {
	var calendar = [];
	var calendarId = 'macron-emmanuel';
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		var now = moment();
		var monthKey = now.format("MMYY");

		const res = await fetch.nSave("http://www.elysee.fr/agenda", calendarId + "-" + monthKey + '.html', {
			insecureHTTPParser: true
		});

		var dataList = await res;

		const {
			JSDOM
		} = require("jsdom");
		const {
			window
		} = new JSDOM(dataList);
		const $ = require("jquery")(window);

		var i = 0;

		$(".container>.container--thicker>div").each(function (index) {
			var id = $(this).attr('id');
			if (id && id.startsWith("d-")) {
				var currentDay = id.replace("d-", "");
				$(this).find(".list-table>li").each(function () {
					var startDate = moment(currentDay + " " + $(this).find(".list-table__details>.list-table__hour").first().text(), "YYMMDD hh:mm");
					var endDate = moment(currentDay + " " + $(this).find(".list-table__details>.list-table__hour").first().text(), "YYMMDD hh:mm").add(1, 'h');
					if (startDate.isValid() && endDate.isValid()) {
						if (startDate.hour() == 0) {
							//console.log($(this).html())
							//console.log(currentDay)
							//console.log(strong)
						} else {
							var content = $(this).find(".list-table__content").first().text().trim();
							var location = '';
							calendar.push({
								id: calendarId + "-" + i,
								calendarId: calendarId,
								title: content,
								category: 'time',
								start: startDate.format(),
								end: endDate.format(),
								location: location,
								attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
							});
							i++;
						}
					}
				});
			}
			if (index == $(".container>.container--thicker>div").length - 1) {
				//console.log(calendar);

				var filePath = path.join(process.cwd(), 'data', 'agendas', calendarId + '.json');
				fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
					if (err) {
						return console.log(calendarId + " " + err);
					} else {
						if (global.env == "dev") {
							console.log("[" + calendarId + "] : " + calendar.length + " events saved !");
						}
					}

				});
			}
		})

	} catch (err) {
		console.error(err);
	}
}

exports.scrap = scrap;
