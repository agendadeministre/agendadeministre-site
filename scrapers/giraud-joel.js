var minCohesionTerritoiresScrapper = require('./common/min-cohesion-territoires');

var calendarId = 'giraud-joel';
var minId = 'joel-giraud';

var scrap = function () {
	return minCohesionTerritoiresScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
