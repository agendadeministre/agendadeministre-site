var minEcoloSolidaireScrapper = require('./common/min-ecologique-solidaire');

var calendarId = 'abba-berangere';
var minId = 'berangere-abba';

var scrap = function () {
	return minEcoloSolidaireScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
