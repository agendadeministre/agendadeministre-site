var minCohesionTerritoiresScrapper = require('./common/min-cohesion-territoires');

var calendarId = 'denormandie-julien';
var minId = 'julien-denormandie';

var scrap = function () {
	return minCohesionTerritoiresScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
