var minCohesionTerritoiresScrapper = require('./common/min-cohesion-territoires');

var calendarId = 'gourault-jacqueline';
var minId = 'jacqueline-gourault';

var scrap = function () {
	return minCohesionTerritoiresScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
