var fetch = require('../../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
moment.locale('fr');

var scrap = async function (calendarId, minId) {
    var calendar = [];
    try {
        var gouv = [];
        var gouvSearch = {};
        var filePath = path.join(process.cwd(), 'data', 'gouv.json');
        const gouvData = await fs.promises.readFile(filePath);
        gouv = JSON.parse(gouvData);
        for (g in gouv) {
            gouvSearch[gouv[g].id] = gouv[g];
        }

        var now = moment();
        if (now.day() == 0) {
            var premierJourDeLaSemaine = now.subtract(1, 'weeks').day(1);
        } else {
            var premierJourDeLaSemaine = now.day(1);
        }
        var weekKey = premierJourDeLaSemaine.format("DDMMYYYY");
        const res = await fetch.nSave('https://www.cohesion-territoires.gouv.fr/agenda-de-' + minId, calendarId + '-' + weekKey + '.txt');

        var data = await res;

        const {
            JSDOM
        } = require("jsdom");
        const {
            window
        } = new JSDOM(data);
        const $ = require("jquery")(window);

        var currentDay = "";
        var i = 0;

        $("article .text-formatted").children().each(function () {
            if ($(this).prop("tagName") == "H2") {
                currentDay = $(this).text();
                if (currentDay.search(/202/g) == -1) {
                    currentDay += " " + moment().year();
                }
            } else if ($(this).prop("tagName") == "P") {
                if ($(this).children("strong").length == 1) {
                    if ($(this).children("strong").first().text() == "Journée") {
                        var startDate = moment(currentDay, "dddd D MMMM yyyy");
                        var endDate = moment(currentDay, "dddd D MMMM yyyy");
                    } else if ($(this).children("strong").first().text() == "Matinée") {
                        var startDate = moment(currentDay + " 9h00", "dddd D MMMM yyyy hh\hmm");
                        var endDate = moment(currentDay + " 12h00", "dddd D MMMM yyyy hh\hmm");
                    } else {
                        var startDate = moment(currentDay + " " + $(this).children("strong").first().text(), "dddd D MMMM yyyy hh\hmm");
                        var endDate = moment(currentDay + " " + $(this).children("strong").first().text(), "dddd D MMMM yyyy hh\hmm").add(1, 'h');
                    }

                    var location = '';
                    if ($(this).children("en").length == 1) {
                        location = $(this).children("en").text();
                    }
                    $(this).children("em, strong").remove();
                    var content = $(this).text().trim();
                    if (startDate.isValid()) {
                        calendar.push({
                            id: calendarId + "-" + i,
                            calendarId: calendarId,
                            title: content,
                            category: 'time',
                            start: startDate.format(),
                            end: endDate.format(),
                            location: location,
                            attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
                        });
                        i++;
                    }
                }
            }
        });
        //console.log(calendar);
        return calendar;
    } catch (err) {
        console.error(err);
    }
}

exports.scrap = scrap;