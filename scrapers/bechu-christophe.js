var minEcoloSolidaireScrapper = require('./common/min-ecologique-solidaire');

var calendarId = 'bechu-christophe';
var minId = 'christophe-bechu';

var scrap = function () {
	return minEcoloSolidaireScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
