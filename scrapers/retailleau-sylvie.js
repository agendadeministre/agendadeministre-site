var openDataSoftScrapper = require('./common/scrap-OpenDataSoft');

var calendarId = 'vidal-frederique';
var dataLink = 'https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-agenda-ministre/download?format=json&timezone=Europe/Berlin&use_labels_for_header=true';

var scrap = function () {
	return openDataSoftScrapper.scrap(calendarId, dataLink);
}

exports.scrap = scrap;
