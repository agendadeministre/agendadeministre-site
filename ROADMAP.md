# Roadmap

- Concernant le site :
  - [ ] Améliorer l’interactivité de l'agenda *(notamment avec les évènements)*.
  - [ ] Gérer l'i18n *(a minima uniformiser la langue utilisée. Actuellement mélange fr/en)*.
  - [ ] Étudier la possibilités de *router* les vues d'un agenda  
    *(par mois et semaine. Par jour cela générerait probablement un nombre trop important de pages. À moins peut-être de faire pour les jours, des routes "client-only", qui ne seront pas générées en statique)*.
  - [ ] Créer une page *routée* pour chaque évènement.  
    *(les pages [sont déjà créée](https://framagit.org/mab/agendadeministre/-/blob/dc005563cdbe4cf963495106a200cc6f26f6a147/plugins/gatsby-plugin-agendadeministre/gatsby-node.js#L37-41), mais sans contenu pour le moment)*
    - [ ] Générer les pages d'évènements pour *tous* les évènements.  
      *(Pour le moment on ne remonte que 3 mois dans le passé pour éviter un trop grand nombre de pages)*
  - [x] Remettre en place une génération d'ICS pour chaque agenda.
  - [x] Permettre l'affichage de plusieurs agendas simultanément *(équivalent du `/gouv` actuel)*.
  - [ ] Générer un lien associatif entre *participants* à un évènement et *titulaires* d'agendas.
    - [ ] Dédoublonner les évènements communs à plusieurs agenda.
  - Ajouter aux titulaires d'agendas des informations institutionnelles et liens (vers sites officiels et Wikipedia).
  - [ ] Permettre une recherche (filtrage dans un premier temps) parmi les évènements d'un agenda
  - [ ] Permettre un filtrage de la liste des agendas
- Concernant les scrapers :
  - [ ] Peut-être en faire un projet indépendant ?  
    *L'idée de les séparer est de pouvoir potentiellement s'en servir sans le site, pour par exemple reverser le résultat (JSON et/ou ICS) en open data*.
  - [ ] Uniformiser le format de date  
    *(il y a actuellement une variation, sans doute lié à un scraper particulier)*.