var fs = require('fs');
const moment = require('moment');

var log = async function (msg) {
	var logPath;
	if (global.logPath) {
		logPath = global.logPath;
	} else {
		logPath = "dev.log";
	}

	fs.appendFile('logs/' + logPath, moment().format() + " : " + JSON.stringify(msg) + "\n",
		function (err) {
			if (err) {
				return console.log(err);
			}

		});

	console.log(msg);

}

exports.log = log;
exports.error = log;