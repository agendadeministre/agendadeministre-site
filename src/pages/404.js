import React from 'react';
import { Link } from 'gatsby-theme-material-ui';
import { Box, Grid, Typography, Alert, AlertTitle } from '@mui/material';
import { ArrowBack } from '@mui/icons-material';
import Layout from '../components/Layout';

const iconSX = {
  marginBottom: '-5px',
  marginRight: '5px',
};

const Error404 = () => {
  return (
    <Layout title="Erreur 404">
      <Grid container alignItems="center">
        <Grid item>
          <Typography variant="h1">
            AgendaDeMinistre.fr
          </Typography>
          <Typography variant="subtitle1">
            Vous avez toujours rêvé d'avoir un agenda de ministre ?
          </Typography>
        </Grid>
      </Grid>

      <Grid container>
        <Box mt={2} width={1}>
          <Alert severity="error">
            <AlertTitle>Erreur 404</AlertTitle>
            Cette page n'existe pas.
          </Alert>
        </Box>
      </Grid>

      <Box mt={2}>
        <Link to="/"><ArrowBack fontSize="small" sx={iconSX} />
          Retour à l'accueil
        </Link>
      </Box>

    </Layout>
  );
};

export default Error404;
