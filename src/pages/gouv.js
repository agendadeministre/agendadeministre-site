import React, { useMemo } from 'react';
import { graphql } from 'gatsby';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import { Box, Typography, Avatar, Grid, Fab, Dialog, DialogContent, DialogActions, Button, Link } from '@mui/material';
import { ArrowBack } from '@mui/icons-material';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'moment/locale/fr';

import Layout from '../components/Layout';
import CustomDialogTitle from '../components/CustomDialogTitle';
import CalendarDownloadIcon from '../components/CalendarDownloadIcon';
import calendari18n from '../lib/shared';

const localizer = momentLocalizer(moment);
const accessor = key => ({ [key]: k }) => moment(k).toDate();
const startAccessor = accessor('start');
const endAccessor = accessor('end');
const headerSX = {
  marginTop: '20px',
  marginBottom: '20px',
};
const iconSX = {
  marginBottom: '-5px',
  marginRight: '5px',
};
const avatarSX = {
  width: '80px!important',
  height: '80px!important',
  marginRight: '20px',
};
const linksSX = {
  marginLeft: 'auto!important',
  alignSelf: 'flex-end',
};
const calendarSX = {
  marginTop: '40px',
  height: 800,
};
const fabSX = {
  position: 'absolute!important',
  bottom: '30px',
  right: '-30px',
  cursor: 'pointer',
};

function EventAgenda({ event }) {
  console.log(event);
  return (
    <span>
      <Link href={'/'+event.calendarId+'/'+event.internal.contentDigest}>{event.title}</Link>
    </span>
  )
}

const Agenda = props => {
  const { data: {
    allEvents: { events = [] } = {},
  } = {} } = props;

  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  
  const { components } = useMemo(
    () => ({
      components: {
        agenda: {
          event: EventAgenda,
        },
      },
    }),
    []
  );

  return (
    <Layout title="Agenda du gouvernement">
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <CustomDialogTitle id="customized-dialog-title" onClose={handleClose}>
          S'abonner à cet agenda
        </CustomDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            Pour s'abonner à  l'agenda du gouvernement, utilisez votre application de calendrier favorite, ajoutez un calendrier en ligne et copiez-collez l'adresse suivante : <code>http://agendadeministre.fr/gouv.ics</code>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            C'est fait !
          </Button>
        </DialogActions>
      </Dialog>

      <Link href="/" underline="hover"><ArrowBack fontSize="small" sx={iconSX} />
        Retour à la liste
      </Link>

      <Grid container alignItems="center" sx={headerSX}>
        <Grid item>
          <Avatar alt="" sx={avatarSX} />
        </Grid>
        <Grid item>
          <Typography variant="h1">
            Gouvernement
          </Typography>
          <Typography variant="subtitle1">
            Le président, tous les ministres et sécrétaires d'État
          </Typography>
        </Grid>
        <Grid item sx={linksSX}>
          <Button variant="contained" onClick={handleClickOpen}>
            S'abonner
          </Button>
        </Grid>
      </Grid>

      <Calendar
        components={components}
        sx={calendarSX}
        localizer={localizer}
        events={events}
        startAccessor={startAccessor}
        endAccessor={endAccessor}
        popup={true}
        defaultView="week"
        min={moment('08:00', 'HH:mm').toDate()}
        max={moment('20:00', 'HH:mm').toDate()}
        messages={calendari18n}
        onSelectEvent={function(event){
          console.log(event);
          window.location = '/'+event.calendarId+'/'+event.internal.contentDigest;
        }}
      />

      <Box container position="relative">
        <Fab color="secondary" aria-label="subscribe" sx={fabSX} onClick={handleClickOpen}>
          <CalendarDownloadIcon />
        </Fab>
      </Box>

    </Layout>
  );
};

export default Agenda;

export const query = graphql`
  query {
    allEvents {
      events: nodes {
        calendarId
        internal {
          contentDigest
        }
        title
        category
        start
        end
        location
        attendees
      }
    }
  }
`;
