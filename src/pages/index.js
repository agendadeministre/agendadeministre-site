import React from 'react';
import { Box, Button, Divider, Grid, Typography } from '@mui/material';
import { Code, Twitter } from '@mui/icons-material';

import Layout from '../components/Layout';
import GouvGrid from '../components/GouvGrid';

export default function Home (props) {
  return (
    <Layout title="Accueil">
      <Grid container alignItems="center">
        <Grid item>
          <Typography variant="h1">
            AgendaDeMinistre.fr
          </Typography>
          <Typography variant="subtitle1">
            Vous avez toujours rêvé d'avoir un agenda de ministre ?
          </Typography>
        </Grid>
      </Grid>

      <GouvGrid />

      <Grid container alignItems="center">
        <Grid item>
          <Divider />
          <Box mt={2} mb={2}>
            <Typography variant="body2">
              AgendaDeMinistre.fr est un site non officiel permettant de centraliser les agendas
              gouvernementaux.
            </Typography>
            <Button href="https://twitter.com/AgendaMinistre" target="_blank">
              <Twitter fontSize="small"/>Bot Twitter
            </Button>
            <Button href="https://framagit.org/agendadeministre" target="_blank">
              <Code fontSize="small"/>Code source
            </Button>
          </Box>
        </Grid>
      </Grid>

    </Layout>
  );
};
