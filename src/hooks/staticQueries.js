import { graphql, useStaticQuery } from 'gatsby';

export const useGouvMembers = () => {
  const { allGouvMember: { gouvMembers = [] } = {} } = useStaticQuery(graphql`
    query useGouvMembers {
      allGouvMember(sort: { fields: nom }) {
        gouvMembers: nodes {
          nom
          prenom
          qualite
          photo
          uid
          civilite
          scrapped
          searchString
        }
      }
    }
  `);

  return gouvMembers;
};

export default useGouvMembers;
