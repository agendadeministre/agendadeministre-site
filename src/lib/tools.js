import slugify from 'slugify';

export const cleanName = str => slugify(str, { lower: true, strict: true });
export const asSearchString = str => cleanName(str).replace(/[^a-z0-9]/g, '');
