const calendari18n = {
  date: 'Date',
  time: 'Heure',
  event: 'Événement',
  allDay: 'Jour entier',
  week: 'Semaine',
  work_week: 'Semaine',
  day: 'Jour',
  month: 'Mois',
  previous: '<<',
  next: '>>',
  yesterday: 'Hier',
  tomorrow: 'Demain',
  today: "Aujourd'hui",
  agenda: 'Agenda',
  noEventsInRange: "Il n'y a aucun événement à afficher.",
  showMore: total => `+${total} en plus`,
};

export default calendari18n;
