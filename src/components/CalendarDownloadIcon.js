import React from 'react';
import { SvgIcon } from '@mui/material';

const CalendarDownloadIcon = () => (
  <SvgIcon>
    <path d="M19 4.5h-1v-2h-2v2H8v-2H6v2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-14c0-1.1-.9-2-2-2zm0 16H5v-11h14v11z" />
    <path d="M11.8 17l3.4-3.5-1-1.1-2.4 2.5-2.5-2.5-1.1 1.1zm-3.6.6H15v1.5H8.2z" />
    <path d="M11 10.6h1.5v4.2H11z" />
  </SvgIcon>
);

export default CalendarDownloadIcon;
