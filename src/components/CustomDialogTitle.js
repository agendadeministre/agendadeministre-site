import React from 'react';
import { IconButton, Typography, DialogTitle } from '@mui/material';
import { Close } from '@mui/icons-material';

const closeButtonSX = {
  position: 'absolute!important',
  right: '10px',
  top: '10px',
  color: '#ddd',
};

const CustomDialogTitle = props => {
  const { children, onClose } = props;
  return (
    <DialogTitle disableTypography>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" sx={closeButtonSX} onClick={onClose}>
          <Close />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

export default CustomDialogTitle;
