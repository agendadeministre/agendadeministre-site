import React from 'react';
import SearchIcon from '@mui/icons-material/Search';
import FormControl from '@mui/material/FormControl';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';

const FilterForm = ({
  value,
  onChange = () => {},
}) => {
  return (
    <FormControl fullWidth>
      <TextField
        id="input-with-icon-textfield"
        label="Rechercher"
        value={value}
        onChange={onChange}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <SearchIcon
                aria-label="Exécuter la recherche"
                onClick={onChange}
                onMouseDown={onChange}
                edge="end"
              />
            </InputAdornment>
          ),
        }}
        variant="outlined"
      />
    </FormControl>
  );
};

export default React.memo(FilterForm);
