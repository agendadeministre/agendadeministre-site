import React from 'react';
import { Typography, Grid, Link, Chip } from '@mui/material';
import { ArrowBack, LocationOn, ArrowForward } from '@mui/icons-material';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'moment/locale/fr';
import Layout from './Layout';

const headerSX = {
  marginTop: '20px',
  marginBottom: '20px',
};
const iconSX = {
  verticalAlign: 'middle',
  marginRight: '5px',
};

function Event ({ pageContext }) {
  const { calendarId, title, start, end, location, attendees  } = pageContext;
  console.log(attendees);
  return (
    <Layout title={`Événement}`}>
      <Link href={'/'+calendarId} underline="hover"><ArrowBack fontSize="small" sx={iconSX} />
        Retour au calendrier
      </Link>

      <Grid container alignItems="center" sx={headerSX}>
        <Grid item>
          <Typography variant="h1">
            Événement
          </Typography>
          <Typography variant="h2">
            Objet
          </Typography>
          <Typography variant="body1">
            {title}
          </Typography>
          <Typography variant="h2">
            Dates
          </Typography>
          <Typography variant="body1">
            {start} <ArrowForward fontSize="small" sx={iconSX} /> {end}
          </Typography>
          <Typography variant="h2">
            Lieu
          </Typography>
          <Typography variant="body1">
            <LocationOn fontSize="small" sx={iconSX} /> {location ?? "Inconnu"}
          </Typography>
          <Typography variant="h2">
            Participants
          </Typography>
          <Typography variant="body1">
            {attendees.map(attendee => <Chip label={attendee}/>)}
          </Typography>
        </Grid>
      </Grid>
    </Layout>
  );
}

export default Event;
