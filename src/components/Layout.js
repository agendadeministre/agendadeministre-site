import React from 'react';
import Helmet from 'react-helmet';
import { Container } from '@mui/material';

const Layout = ({title = '', ...props }) => {
  return (
    <Container maxWidth="lg">
      <Helmet
        htmlAttributes={{ lang: 'fr' }}
        title={title}
        titleTemplate="%s | AgendaDeMinistre"
      />
      <div
        {...props}
      />
    </Container>
  );
};

export default Layout;
