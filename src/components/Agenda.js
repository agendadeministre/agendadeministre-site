import React from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import { Box, Typography, Avatar, Grid, Tooltip, Fab, Dialog, DialogContent, DialogActions, Button, Link } from '@mui/material';
import { ArrowBack, EventNote } from '@mui/icons-material';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'moment/locale/fr';

import Layout from './Layout';
import CustomDialogTitle from './CustomDialogTitle';
import CalendarDownloadIcon from './CalendarDownloadIcon';
import calendari18n from '../lib/shared';

const localizer = momentLocalizer(moment);
const accessor = key => ({ [key]: k }) => moment(k).toDate();
const startAccessor = accessor('start');
const endAccessor = accessor('end');
const headerSX = {
  marginTop: '20px',
  marginBottom: '20px',
};
const iconSX = {
  verticalAlign: 'middle',
  marginRight: '5px',
};
const avatarSX = {
  width: '80px!important',
  height: '80px!important',
  marginRight: '20px',
};
const linksSX = {
  marginLeft: 'auto!important',
  alignSelf: 'flex-end',
};
const calendarSX = {
  marginTop: '40px',
  height: 800,
};
const footerSX = {
  textAlign: 'center',
  marginTop: '40px',
  marginBottom: '40px',
};
const sourceIconSX = {
  verticalAlign: 'middle',
  marginRight: '5px',
};
const fabSX = {
  position: 'absolute!important',
  bottom: '30px',
  right: '-30px',
  cursor: 'pointer',
};

function Agenda ({ pageContext }) {
  const { uid, civilite, nom, prenom, qualite, photo, memberEvents } = pageContext;
  const events = memberEvents || [];

  const { type, site } = {};

  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Layout title={`Agenda de ${civilite} ${prenom} ${nom}`}>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <CustomDialogTitle id="customized-dialog-title" onClose={handleClose}>
          S'abonner à cet agenda
        </CustomDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            Pour s'abonner à  l'agenda de {civilite} {prenom} {nom}, utilisez votre application de calendrier favorite, ajoutez un calendrier en ligne et copiez-collez l'adresse suivante : <code>http://agendadeministre.fr/{uid}.ics</code>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            C'est fait !
          </Button>
        </DialogActions>
      </Dialog>

      <Link href="/" underline="hover"><ArrowBack fontSize="small" sx={iconSX} />
        Retour à la liste
      </Link>

      <Grid container alignItems="center" sx={headerSX}>
        <Grid item>
          <Avatar
            alt={`Photo de ${civilite} ${prenom} ${nom}`}
            src={photo}
            sx={avatarSX}
          />
        </Grid>
        <Grid item>
          <Typography variant="h1">
            {civilite} {prenom} {nom}
          </Typography>
          <Typography variant="subtitle1">
            {qualite}
          </Typography>
        </Grid>
        <Grid item sx={linksSX}>
          <Button variant="contained" onClick={handleClickOpen}>
            S'abonner
          </Button>
        </Grid>
      </Grid>

      <Calendar
        sx={calendarSX}
        localizer={localizer}
        events={events}
        startAccessor={startAccessor}
        endAccessor={endAccessor}
        popup={true}
        defaultView="week"
        min={moment('08:00', 'HH:mm').toDate()}
        max={moment('20:00', 'HH:mm').toDate()}
        messages={calendari18n}
        onSelectEvent={function(event){
          window.location = '/'+uid+'/'+event.internal.contentDigest;
        }}
      />

      <Box container position="relative">
        <Fab color="secondary" aria-label="subscribe" sx={fabSX} onClick={handleClickOpen}>
          <CalendarDownloadIcon />
        </Fab>
      </Box>

      <footer sx={footerSX}>
        {site && (
          <Tooltip title={`Format : ${type}`}>
            <Link href={site}>
              <EventNote
                fontSize="small"
                sx={sourceIconSX}
              />
              Source des données
            </Link>
          </Tooltip>
        )}
      </footer>
    </Layout>
  );
}

export default Agenda;
