import React from 'react';
import { Grid, Paper, Avatar, Typography, Link } from '@mui/material';
import FilterForm from './FilterForm';
import useGouvMembers from '../hooks/staticQueries';

import { asSearchString } from '../lib/tools';

const paperSX = {
  height: "100%", p: "20px",
  "&:hover": {
    boxShadow: 3,
  },
};

const GouvGrid = (props) => {
  const gouvMembers = useGouvMembers();
  const [textFilter, setTextFilter] = React.useState('');

  const handleChange = React.useCallback(({ target: { value } = {} }) =>
    setTextFilter(value), []);

  const clearFilter = React.useCallback(() => setTextFilter(''), []);

  const memberFilter = React.useCallback(
    ({ searchString = '' }) => searchString.includes(asSearchString(textFilter)),
    [textFilter],
  );

  const filteredGouvMembers = React.useMemo(
    () => gouvMembers.filter(memberFilter),
    [gouvMembers, memberFilter],
  );

  return (
    <>
      <FilterForm
        value={textFilter}
        onChange={handleChange}
        onClear={clearFilter}
      />

      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
        sx={{ pt: '20px', pb: '20px' }}
      >
        {filteredGouvMembers.map(({ nom, prenom, uid, photo, civilite, qualite, scrapped }) => (
          <Grid
            item
            key={uid}
            xs={4}
          >
            <Link key={uid} href={uid} underline="none" variant="inherit">
              <Paper sx={paperSX}>
                <Grid container alignItems="center" wrap="nowrap">
                  <Grid item sx={{ mr: "10px" }}>
                    <Avatar alt="Photo de {prenom} {nom}" src={photo} />
                  </Grid>
                  <Grid item>
                    <Typography variant="body1">{civilite} {prenom} {nom}</Typography>
                  </Grid>
                </Grid>

                <Typography variant="caption">{qualite}</Typography>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>

      <Grid container>
        <Grid xs={12}>
          <Link href="/gouv" underline="none" variant="inherit">
            <Paper sx={paperSX}>
              <Grid container alignItems="center">
                <Grid item sx={{ mr: "10px" }}>
                  <Avatar alt="Gouvernement" />
                </Grid>
                <Grid item>
                  <Typography variant="body1">Tout le gouvernement</Typography>
                </Grid>
              </Grid>
            </Paper>
          </Link>
        </Grid>
      </Grid>
    </>
  );
};

export default GouvGrid;
