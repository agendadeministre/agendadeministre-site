import { createTheme, responsiveFontSizes } from '@mui/material';
import { yellow, grey } from '@mui/material/colors';

/**
 * https://material-ui.com/customization/default-theme/
 *
 * https://material-ui.com/customization/globals/
 * https://material-ui.com/customization/palette/
 * https://material-ui.com/customization/typography/
 *
 * https://material-ui.com/customization/color/
 */
const theme = createTheme({
  typography: {
    // fontSize: 14,
    h1: { fontSize: '3rem' },
    h2: { fontSize: '2.125rem' },
    h3: { fontSize: '1.5rem' },
    h4: { fontSize: '1.25rem' },
    h5: { fontSize: '1.125rem' },
    h6: { fontSize: '1rem' },
  },
  palette: {
    primary: {
      main: grey[800],
    },
    secondary: {
      main: yellow[600],
    },
  },
});

export default responsiveFontSizes(theme);
