/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  plugins: [
    'gatsby-theme-material-ui',
    'gatsby-source-agendadeministre',
    'gatsby-plugin-agendadeministre',
    {
      resolve: 'gatsby-transformer-json',
      options: {
        typeName: ({ node: { sourceInstanceName } }) => sourceInstanceName,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'events',
        path: `${__dirname}/data/agendas/`,
      },
    },
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'AgendaDeMinistre.fr',
        short_name: 'AgendaDeMinistre',
        start_url: '/',
        background_color: '#424242',
        theme_color: '#fdd835',
        display: 'standalone',
        icon: 'src/images/icon.png',
        crossOrigin: 'use-credentials',
      },
    },
  ],
};
