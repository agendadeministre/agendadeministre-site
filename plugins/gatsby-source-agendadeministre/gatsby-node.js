const fs = require('fs').promises;
const path = require('path');
const slugify = require('slugify');

slugify.extend({ '/': '-' });
const cleanName = str => slugify(str, { lower: true, strict: true });
const makeSearchString = object => cleanName(Object.values(object).join` `)
  .replace(/[^a-z0-9]/g, '');

exports.sourceNodes = async ({
  actions: { createNode },
  createNodeId,
  createContentDigest,
}) => {
  const gouvBuffer = await fs.readFile(path.join(process.cwd(), 'data', 'gouv.json'));
  const gouvJson = gouvBuffer.toString();
  const gouv = JSON.parse(gouvJson);

  gouv.forEach(gouvMember => {
    const uid = gouvMember.id;
    const id = createNodeId(`gouv-member-${uid}`);

    const { photo, ...searchFields } = gouvMember;

    const node = {
      ...gouvMember,
      searchString: makeSearchString(searchFields),
      uid,
      id,
      parent: null,
      children: [],
      internal: {
        type: 'GouvMember',
        content: JSON.stringify(gouvMember),
        contentDigest: createContentDigest(gouvMember),
      },
    };

    createNode(node);
  });
};
