const path = require('path');
const moment = require('moment');
const ical = require('ical-generator');
const console = require('../../lib/log-n-save');

exports.createPages = async ({
  graphql,
  actions: { createPage },
}) => {
  const queryResults = await graphql(`
    query {
      allEvents {
        events: nodes {
          calendarId
          start
          end
          title
          attendees
          internal {
            contentDigest
          }
        }
      }
      allGouvMember {
        gouvMembers: nodes {
          nom
          prenom
          qualite
          uid
          civilite
          photo
        }
      }
    }
  `);

  const {
    data: {
      allEvents: { events = [] } = {},
      allGouvMember: { gouvMembers = [] } = {},
    } = {},
  } = queryResults;

  const min = moment(new Date()).subtract(3, 'months');

  // Keep only event from three months past
  const recentEvents = events.filter(({ start }) => moment(start, 'YYYY-MM').isAfter(min));

  /**
   * Create page for each event
   */
  recentEvents
    .forEach(({ calendarId, internal: { contentDigest }, title, category, start, end, location, attendees = {} }) => {
      createPage({
        path: path.join(calendarId, contentDigest),
        component: path.resolve('src/components/Event.js'),
        context: { calendarId, title, category, start, end, location, attendees },
      });
    });

  /**
   * Create .ics & page for gouvMembers
   */
  gouvMembers
    .forEach(({ uid, civilite, nom, prenom, qualite, photo }) => {
      const memberEvents = recentEvents.filter(({ calendarId }) => calendarId === uid);
      const calendar = ical({
        domain: 'agendadeministre.fr',
        name: `Agenda de ${civilite} ${prenom} ${nom} (${qualite})`,
      });
      createPage({
        path: uid,
        component: path.resolve('src/components/Agenda.js'),
        context: { uid, civilite, nom, prenom, qualite, photo, memberEvents },
      });

      memberEvents.forEach(({ start, end, title: summary, location, attendees }) => {
        try {
          calendar.createEvent({
            start,
            end,
            summary,
            location,
            attendees: attendees.map(name => ({ email: 'noreply@agendadeministre.fr', name })),
          });
        } catch (e) {
          console.log(e);
        }
      });

      calendar.saveSync(path.join('public', `${uid}.ics`));
    });

  /**
   * Create gouv.ics
   */
  const calendar = ical({
    domain: 'agendadeministre.fr',
    name: 'Agenda du gouvernement',
  });
  recentEvents.forEach(({ start, end, title: summary, location, attendees }) => {
    try {
      calendar.createEvent({
        start,
        end,
        summary,
        location,
        attendees: attendees.map(name => ({ email: 'noreply@agendadeministre.fr', name })),
      });
    } catch (e) {
      console.log(e);
    }
  });

  calendar.saveSync(path.join('public', 'gouv.ics'));
};
