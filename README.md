# AgendaDeMinistre

> Vous avez toujours rêvé d'avoir un agenda de ministre ?

## Installation

Clonez :
```
git clone https://framagit.org/agendadeministre/agendadeministre-site.git
```

Installez :
```
npm install
```

Il vous faudra peut-être installer Browserify et Gatsby.
```
npm install -g browserify gatsby
```

Démarrez le projet en local :
```
npm start
```

Et voilà, le serveur est démarré sur `http://localhost:8000` !


Pour la prod, utilisez Systemd :
```
sudo cp config/agendadeministre.service /lib/systemd/system/agendadeministre.service

systemctl daemon-reload

systemctl start agendadeministre

journalctl -u agendadeministre.service
```


## Contribuer

Toutes contributions sont les bienvenues ! Par exemple, vous pouvez coder des scrappeur.
